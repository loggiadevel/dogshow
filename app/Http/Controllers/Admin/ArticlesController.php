<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Article;
use Storage;
use App\Http\Requests\ArticleRequest;
use Session;

class ArticlesController extends Controller {

    public function index() {                    
        $articles = Article::paginate(7);
        return view('admin.articles.index', compact('articles'));
    }

    public function create() {
        return view('admin.articles.create');
    }

    public function store(ArticleRequest $request) {

        $article = new Article;
        $article->title = $request->title;
        $article->description = $request->description;
        $article->status = $request->status;
        $article->save();

        //pega a extensao da imagem
        $extension = $request->file('img')->getClientOriginalExtension();

        Storage::put(
                'articles/' . $article->id . '.' . $extension, file_get_contents($request->file('img')->getRealPath())
        );

        $article->img = 'storage/articles/' . $article->id . '.' . $extension;
        $article->save();

        return redirect('admin/articles');
    }

    public function delete($id) {
        $article = Article::find($id);                        
        $article->delete();
        return redirect('admin/articles');
    }

    public function edit($id) {
        $article = Article::find($id);
        return view('admin/articles/edit', compact('article'));
    }

    public function update(ArticleRequest $request) {

        $id = $request->input('id');
        $article = Article::find($id);
        $article->title = $request->title;
        $article->description = $request->description;
        $article->status = $request->status;
        $article->save();

        //pega a extensao da imagem
        $extension = $request->file('img')->getClientOriginalExtension();

        Storage::put(
                'articles/' . $article->id . '.' . $extension, file_get_contents($request->file('img')->getRealPath())
        );

        $article->img = 'storage/articles/' . $article->id . '.' . $extension;
        $article->save();

        Session::flash('message', 'Artigo atualizado com sucesso!');

        return back()->withInput();
    }

}
