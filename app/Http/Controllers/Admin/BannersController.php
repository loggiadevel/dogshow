<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Banner;
use App\Http\Requests\BannerRequest;
use Storage;
use Session;

class BannersController extends Controller
{
    public function index() {

        $banners = Banner::paginate(7);

        return view('admin.banners.index', compact('banners'));
    }
    
    
    public function create() {
        return view('admin.banners.create');
    }

    public function store(BannerRequest $request) {

        $banner = new Banner;
        $banner->title = $request->title;
        $banner->description = $request->description;
        $banner->link = $request->link;
        $banner->status = $request->status;
        $banner->save();

        //pega a extensao da imagem
        $extension = $request->file('img')->getClientOriginalExtension();

        //grava imagem na pasta banners
        Storage::put(
                'banners/' . $banner->id . '.' . $extension, file_get_contents($request->file('img')->getRealPath())
        );

        $banner->img = 'storage/banners/' . $banner->id . '.' . $extension;
        $banner->save();

        return redirect('admin/banners');
    }
    
    public function delete($id) {        
         $banner = Banner::find($id);
         $banner->delete();
         return redirect('admin/banners');
    }
    
    
    public function edit($id) {                
        $banner = Banner::find($id);         
        return view('admin/banners/edit', compact('banner'));
    }
    
    public function update(BannerRequest $request) {                
        
        $id = $request->input('id');       
        $banner = Banner::find($id);
        $banner->title = $request->title;
        $banner->description = $request->description;
        $banner->link = $request->link;
        $banner->status = $request->status;
        $banner->save();

        //pega a extensao da imagem
        $extension = $request->file('img')->getClientOriginalExtension();

        Storage::put(
                'banners/' . $banner->id . '.' . $extension, file_get_contents($request->file('img')->getRealPath())
        );

        $banner->img = 'storage/banners/' . $banner->id . '.' . $extension;
        $banner->save();
        
        Session::flash('message', 'Banner atualizado com sucesso!');
                        
        return back()->withInput();
    }
}
