<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Product;
use Storage;
use App\Http\Requests\ProductRequest;
use Session;

class ProductsController extends Controller {

    public function index() {

        $products = Product::paginate(7);

        return view('admin.products.index', compact('products'));
    }

    public function create() {
        return view('admin.products.create');
    }

    public function store(ProductRequest $request) {

        $product = new Product;
        $product->name = $request->name;
        $product->description1 = $request->description1;        
        $product->description2 = $request->description2;
        $product->price = $request->price;
        $product->categories_id = $request->category;
        $product->status = $request->status;
        $product->save();

        //pega a extensao da imagem
        $extension = $request->file('img')->getClientOriginalExtension();

        Storage::put(
                'products/' . $product->id . '.' . $extension, file_get_contents($request->file('img')->getRealPath())
        );

        $product->img = 'storage/products/' . $product->id . '.' . $extension;
        $product->save();

        return redirect('admin/products');
    }

    public function delete($id) {

        $product = Product::find($id);
        $product->delete();
        return redirect('admin/products');
    }

    public function edit($id) {
        $product = Product::find($id);
        return view('admin/products/edit', compact('product'));
    }

    public function update(ProductRequest $request) {

        $id = $request->input('id');
        $product = Product::find($id);
        $product->name = $request->name;
        $product->description1 = $request->description1;
        $product->description2 = $request->description2;        
        $product->price = $request->price;
        $product->categories_id = $request->category;
        $product->status = $request->status;
        $product->save();

        //pega a extensao da imagem
        $extension = $request->file('img')->getClientOriginalExtension();

        Storage::put(
                'products/' . $product->id . '.' . $extension, file_get_contents($request->file('img')->getRealPath())
        );

        $product->img = 'storage/$products/' . $product->id . '.' . $extension;
        $product->save();

        Session::flash('message', 'Produto atualizado com sucesso!');

        return back()->withInput();
    }

}
