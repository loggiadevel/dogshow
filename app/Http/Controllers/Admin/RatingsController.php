<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Rating;
use Storage;
use App\Http\Requests\RatingRequest;
use Session;

class RatingsController extends Controller {

    public function index() {

        $ratings = Rating::paginate(7);

        return view('admin.ratings.index', compact('ratings'));
    }

    public function create() {
        return view('admin.ratings.create');
    }

    public function store(RatingRequest $request) {                

        $rating = new Rating;
        $rating->name = $request->name;
        $rating->description = $request->description;
        $rating->value = $request->value;
        $rating->save();        

        return redirect('/');
    }

    public function delete($id) {

        $rating = Rating::find($id);
        $rating->delete();
        return redirect('admin/ratings');
    }

    public function edit($id) {
        $rating = Rating::find($id);
        return view('admin/ratings/edit', compact('article'));
    }

    public function update(RatingRequest $request) {

        $id = $request->input('id');
        $rating = Rating::find($id);
        $rating->name = $request->name;
        $rating->description = $request->description;
        $rating->value = $request->value;
        $rating->save();      

        Session::flash('message', 'Artigo atualizado com sucesso!');

        return back()->withInput();
    }

}
