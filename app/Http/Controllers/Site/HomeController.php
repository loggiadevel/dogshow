<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Banner;
use App\Product;
use App\Article;
use App\Rating;

class HomeController extends Controller
{   
    /**
     * Exibe a pagina principal
     */
    public function index()
    {        
        $banners = Banner::all();
        $products = Product::all();
        $articles = Article::all();
        $ratings = Rating::all();
        
        return view('site.home', compact('banners', 'products', 'articles', 'ratings'));
    }
    
}
