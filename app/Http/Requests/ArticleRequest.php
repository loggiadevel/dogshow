<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ArticleRequest extends Request {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'title' => 'required|max:40',
            'description' => 'required',
            'img' => 'required'
        ];
    }

    public function messages() {
        return [
            'title.required' => 'O título do artigo é obrigatório',
            //'title.unique' => 'Ja existe um artigo com este título',
            'title.max' => 'Tamanho máximo para o título é de 20 caracteres',
            'description.required' => 'O descrição do artigo é obrigatório',
            'img.required' => 'A imagem do artigo é obrigatório',
        ];
    }

}
