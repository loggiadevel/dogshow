<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class BannerRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'title' => 'required|max:40',
            'description' => 'required',
            'img' => 'required',
            'link' => 'required'
        ];
    }

    public function messages() {
  
        return [
            'title.required' => 'O titlo do artigo é obrigatório',            
            'title.max' => 'Tamanho máximo para o título é de 20 caracteres',
            'description.required' => 'O descrição do banner é obrigatório',
            'img.required' => 'A imagem do banner é obrigatório',
            'link.required' => 'O link do banner é obrigatório',       
        ];
    }
}
