<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ProductRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'name' => 'required|max:20',
            'description1' => 'required',
            'description2' => 'required',
            'price' => 'required',
            'img' => 'required',
            'category' => 'required',
            'status' => 'required'
        ];
    }

    public function messages() {
  
        return [
            'name.required' => 'O nome do produto é obrigatório',
            'name.max' => 'Tamanho máximo para o nome do produto é de 20 caracteres',
            'description1.required' => 'A descrição do produto é obrigatório',
            'description2.required' => 'A descrição completa do produto é obrigatório',
            'price.required' => 'O preço do produto é obrigatório',            
            'img.required' => 'A imagem do produto é obrigatório',
            'category.required' => 'A categoria do produto é obrigatório'
        ];
    }
}
