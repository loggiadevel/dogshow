<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class RatingRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

   /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'name' => 'required|max:40',
            'description' => 'required',
            'value' => 'required'                                    
        ];
    }

    public function messages() {
  
        return [
            'name.required' => 'O nome é obrigatório',            
            'description.required' => 'A descrição da avaliação é obrigatória',
            'value.required' => 'A nota da avaliação é obrigatória'          
        ];
    }
}
