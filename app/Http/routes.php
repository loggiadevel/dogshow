<?php

//Rotas do site
Route::get('/', ['uses' => 'Site\HomeController@index']);
Route::get('/home', ['uses' => 'Site\HomeController@index']);
Route::post('ratings-store', ['uses' => 'Admin\RatingsController@store']);

//Rotas de login
Route::auth();

//Necessário estar altenticado
Route::group(['middleware' => 'auth'], function () {

    //Rotas da area admin
    Route::group(['prefix' => 'admin'], function () {

        //pagina inicial da area admin
        Route::get('dashboard', function () {
            return view('admin.dashboard.index');
        });

        //rotas dos artigos
        Route::get('articles', ['uses' => 'Admin\ArticlesController@index']);
        Route::get('articles-create', ['uses' => 'Admin\ArticlesController@create']);
        Route::post('articles-store', ['uses' => 'Admin\ArticlesController@store']);
        Route::get('articles-edit/{id}', ['uses' => 'Admin\ArticlesController@edit']);
        Route::post('articles-update', ['uses' => 'Admin\ArticlesController@update']);
        Route::get('articles-delete/{id}', ['uses' => 'Admin\ArticlesController@delete']);


        //rotas dos banners
        Route::get('banners', ['uses' => 'Admin\BannersController@index']);
        Route::get('banners-create', ['uses' => 'Admin\BannersController@create']);
        Route::post('banners-store', ['uses' => 'Admin\BannersController@store']);
        Route::get('banners-edit/{id}', ['uses' => 'Admin\BannersController@edit']);
        Route::post('banners-update', ['uses' => 'Admin\BannersController@update']);
        Route::get('banners-delete/{id}', ['uses' => 'Admin\BannersController@delete']);


        //rotas dos products
        Route::get('products', ['uses' => 'Admin\ProductsController@index']);
        Route::get('products-create', ['uses' => 'Admin\ProductsController@create']);
        Route::post('products-store', ['uses' => 'Admin\ProductsController@store']);
        Route::get('products-edit/{id}', ['uses' => 'Admin\ProductsController@edit']);
        Route::post('products-update', ['uses' => 'Admin\ProductsController@update']);
        Route::get('products-delete/{id}', ['uses' => 'Admin\ProductsController@delete']);


        //rotas dos ratings
        Route::get('ratings', ['uses' => 'Admin\RatingsController@index']);              
        Route::get('ratings-delete/{id}', ['uses' => 'Admin\RatingsController@delete']);
    });
});

