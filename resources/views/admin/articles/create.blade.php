@extends('admin.layouts.master')
@section('content')

<div class="container">
    <div class="row">
        <br/>
        <br/>
        <br/>
        <br>

        <div class="well well-sm">Criar artigos</div>

        <form action="{{ url('admin/articles-store')}}" method="post" method="post" enctype="multipart/form-data">

            <input type="hidden" name="_token" value="{{ csrf_token() }}">

            <div class="form-group">
                <label for="title">Título</label>
                <input type="text" class="form-control" value="{{old('title') }}" name="title" id="title" placeholder="Título">
            </div>
            <div class="form-group">
                <label for="description">Descrição</label>
                <input type="text" class="form-control" value="{{ old('description') }}" name="description" id="description" placeholder="Descrição">
            </div>

            <div class="form-group">
                <label for="img">Imagem</label>
                <input type="file" class="form-control" name="img" id="img" placeholder="Imagem">
            </div>

            <div class="form-group">
                <label for="status">Status</label>
                <select name="status" id="status" class="form-control">
                    <option value="1">Ativo</option>
                    <option value="2">Inativo</option>
                </select>
            </div>

            <button type="submit" class="btn btn-primary">Cadastrar</button>
        </form>

        @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
        
        <br/>
        <br/>
        <br/>
        <br>

    </div>
</div>

@endsection
