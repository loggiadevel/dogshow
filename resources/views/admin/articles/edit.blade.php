@extends('admin.layouts.master')
@section('content')

<div class="container">
    <div class="row">
        <br/>
        <br/>
        <br/>
        <br>

        <div class="well well-sm">Atualizar artigo - {{ $article->id }}</div>

        <form action="{{ url('admin/articles-update')}}" method="post" method="post" enctype="multipart/form-data">

            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" name="id" id="id" value="{{ $article->id }}">

            <div class="form-group">
                <label for="titulo">Título</label>
                <input type="text" class="form-control" value="{{ $article->title }}" name="title" id="title" placeholder="Título">
            </div>
            <div class="form-group">
                <label for="Descrição">Descrição</label>
                <input type="text" class="form-control" value="{{ $article->description }}" name="description" id="description" placeholder="Descrição">
            </div>

            <div class="form-group">
                <label for="img">Imagem</label>
                <input type="file" class="form-control" value="{{ $article->img }}" name="img" id="img" placeholder="Imagem">
            </div>

            <div class="form-group">
                <label for="status">Status</label>
                <select name="status" id="status" class="form-control">                    
                    <option value="1" @if ($article->status == 1) selected="selected" @endif>Ativo</option>
                    <option value="2" @if ($article->status == 2) selected="selected" @endif>Inativo</option>
                </select>
            </div>

            <button type="submit" class="btn btn-primary">Salvar</button>
        </form>

        @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif

        
        @if (Session::has('message'))        
            <div class="alert alert-success">
                <ul>
                    <li class="alert-success">{{ Session::get('message') }}</li>
                </ul>
            </div>
        @endif
        
        <br/>
        <br/>
        <br/>
        <br>

    </div>
</div>

@endsection
