@extends('admin.layouts.master')
@section('content')

<div class="container">
    <div class="row">
        <br/>
        <br/>
        <br/>
        <br>
        <div class="well well-sm">Listagem de artigos <a href="{{ url('admin/articles-create') }}" class="btn btn-primary">Novo</a></div>
        <table class="table table-condensed table-hover table-bordered">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>TÍTULO</th>
                    <th>DESCRIÇÃO</th>
                    <th>IMAGEM</th>
                    <th>STATUS</th>
                    <th>AÇÃO</th>
                </tr>

            </thead>
            <tbody>
                @foreach($articles as $article)
                <tr>
                    <td>{{ $article->id }}</td>
                    <td>{{ $article->title }}</td>
                    <td>{{ $article->description }}</td>
                    <td>{{ $article->img }}</td>
                    <td>{{ $article->status }}</td>
                    <td>
                        <a href="{{ url('admin/articles-edit', [$article->id]) }}" class="btn btn-primary">Editar</a>
                        <a href="{{ url('admin/articles-delete', [$article->id]) }}" class="btn btn-danger">Deletar</a>
                    </td>
                </tr>
                @endforeach
            </tbody>
            
               
            
        </table>
        <div class="text-right">
         {{ $articles->links() }}
        </di>
        
        <br/>
        <br/>
        <br/>
        <br>
        
    </div>
</div>

@endsection
