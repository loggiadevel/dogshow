@extends('admin.layouts.master')
@section('content')

<div class="container">
    <div class="row">
        <br/>
        <br/>
        <br/>
        <br>
        <div class="well well-sm">Listagem de banners <a href="{{ url('admin/banners-create') }}" class="btn btn-primary">Novo</a></div>
        <table class="table table-condensed table-hover table-bordered">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>TÍTULO</th>
                    <th>DESCRIÇÃO</th>
                    <th>IMAGEM</th>
                    <th>LINK</th>
                    <th>STATUS</th>
                    <th>AÇÃO</th>
                </tr>

            </thead>
            <tbody>
                @foreach($banners as $banner)
                <tr>
                    <td>{{ $banner->id }}</td>
                    <td>{{ $banner->title }}</td>
                    <td>{{ $banner->description }}</td>
                    <td>{{ $banner->img }}</td>
                    <td>{{ $banner->link }}</td>
                    <td>{{ $banner->status }}</td>
                    <td>
                        <a href="{{ url('admin/banners-edit', [$banner->id]) }}" class="btn btn-primary">Editar</a>
                        <a href="{{ url('admin/banners-delete', [$banner->id]) }}" class="btn btn-danger">Deletar</a>
                    </td>
                </tr>
                @endforeach
            </tbody>
            
               
            
        </table>
        <div class="text-right">
         {{ $banners->links() }}
        </di>
        
        <br/>
        <br/>
        <br/>
        <br>
        
    </div>
</div>

@endsection
