@extends('admin.layouts.master')
@section('content')

<div class="container">
    <div class="row">
        <br/>
        <br/>
        <br/>
        <br>

        <div class="well well-sm">Cadastrar produto</div>

        <form action="{{ url('admin/products-store')}}" method="post" method="post" enctype="multipart/form-data">

            <input type="hidden" name="_token" value="{{ csrf_token() }}">

            <div class="form-group">
                <label for="name">Nome</label>
                <input type="text" class="form-control" value="{{old('name') }}" name="name" id="title" placeholder="Nome">
            </div>
            <div class="form-group">
                <label for="description1">Descrição Simples</label>
                <input type="text" class="form-control" value="{{ old('description1') }}" name="description1" id="description" placeholder="Descrição Simples">
            </div>
            
            <div class="form-group">
                <label for="description2">Descrição Completa</label>
                <textarea rows="4" cols="50" class="form-control" value="{{ old('description2') }}" name="description2" id="description" placeholder="Descrição Completa"></textarea>
            </div>
            
            <div class="form-group">
                <label for="price">Preço</label>
                <input type="text" class="form-control" value="{{ old('price') }}" name="price" id="description" placeholder="Preço">
            </div>

            <div class="form-group">
                <label for="img">Imagem</label>
                <input type="file" class="form-control" name="img" id="img" placeholder="Imagem">
            </div>
            
            
            <div class="form-group">
                <label for="category">Categoria</label>
                <select name="category" id="category" class="form-control">
                    <option value="1">Ração</option>
                    <option value="2">Utencílios</option>
                    <option value="3">Medicamentos</option>
                </select>
            </div>

            <div class="form-group">
                <label for="status">Status</label>
                <select name="status" id="status" class="form-control">
                    <option value="1">Ativo</option>
                    <option value="2">Inativo</option>
                </select>
            </div>

            <button type="submit" class="btn btn-primary">Cadastrar</button>
        </form>

        @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
        
        <br/>
        <br/>
        <br/>
        <br/>

    </div>
</div>

@endsection
