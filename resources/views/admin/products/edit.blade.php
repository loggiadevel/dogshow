@extends('admin.layouts.master')
@section('content')

<div class="container">
    <div class="row">
        <br/>
        <br/>
        <br/>
        <br>

        <div class="well well-sm">Atualizar produto - {{ $product->id }}</div>

        <form action="{{ url('admin/products-update')}}" method="post" method="post" enctype="multipart/form-data">

            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" name="id" id="id" value="{{ $product->id }}">

            <div class="form-group">
                <label for="name">Nome</label>
                <input type="text" class="form-control" value="{{ $product->name }}" name="name" id="name" placeholder="Nome">
            </div>
            <div class="form-group">
                <label for="description1">Descrição Simples</label>
                <input type="text" class="form-control" value="{{ $product->description1 }}" name="description1" id="description1" placeholder="Descrição Simples">
            </div>
            
            <div class="form-group">
                <label for="description2">Descrição Completa</label>
                <input type="text" class="form-control" value="{{ $product->description2 }}" name="description2" id="description2" placeholder="Descrição Completa">
            </div>
            
            <div class="form-group">
                <label for="price">Preço</label>
                <input type="text" class="form-control" value="{{ $product->price }}" name="price" id="description" placeholder="Preço">
            </div>

            <div class="form-group">
                <label for="img">Imagem</label>
                <input type="file" class="form-control" value="{{ $product->img }}" name="img" id="img" placeholder="Imagem">
            </div>
            
            <div class="form-group">
                <label for="category">Categoria</label>
                <select name="category" id="status" class="form-control">
                    <option value="1" @if ($product->categories_id == 1) selected="selected" @endif>Ração</option>
                    <option value="2" @if ($product->categories_id == 2) selected="selected" @endif>Utencílios</option>
                    <option value="3" @if ($product->categories_id == 3) selected="selected" @endif>Medicamentos</option>                                                            
                </select>
            </div>

            <div class="form-group">
                <label for="status">Status</label>
                <select name="status" id="status" class="form-control">                    
                    <option value="1" @if ($product->status == 1) selected="selected" @endif>Ativo</option>
                    <option value="2" @if ($product->status == 2) selected="selected" @endif>Inativo</option>
                </select>
            </div>

            <button type="submit" class="btn btn-primary">Salvar</button>
        </form>

        @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif

        
        @if (Session::has('message'))        
            <div class="alert alert-success">
                <ul>
                    <li class="alert-success">{{ Session::get('message') }}</li>
                </ul>
            </div>
        @endif
        
        <br/>
        <br/>
        <br/>
        <br/>

    </div>
</div>

@endsection
