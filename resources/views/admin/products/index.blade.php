@extends('admin.layouts.master')
@section('content')

<div class="container">
    <div class="row">
        <br/>
        <br/>
        <br/>
        <br>
        <div class="well well-sm">Listagem de produtos <a href="{{ url('admin/products-create') }}" class="btn btn-primary">Novo</a></div>
        <table class="table table-condensed table-hover table-bordered">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>NOME</th>
                    <th>DESCRIÇÃO SIMPLES</th>
                    <th>DESCRIÇÃO COMPLETA</th>
                    <th>PREÇO</th>                    
                    <th>IMAGEM</th>
                    <th>CATEGORIA</th>
                    <th>STATUS</th>
                    <th>AÇÃO</th>
                </tr>

            </thead>
            <tbody>
                @foreach($products as $product)
                <tr>
                    <td>{{ $product->id }}</td>
                    <td>{{ $product->name }}</td>
                    <td>{{ $product->description1 }}</td>
                    <td>{{ $product->description2 }}</td>
                    <td>{{ $product->price }}</td>
                    <td>{{ $product->img }}</td>
                    <td>{{ $product->categories_id }}</td>
                    <td>{{ $product->status }}</td>

                    <td>
                    <a href="{{ url('admin/products-edit', [$product->id]) }}" class="btn btn-primary">Editar</a>
                        <a href="{{ url('admin/products-delete', [$product->id]) }}" class="btn btn-danger">Deletar</a>
                    </td>
                </tr>
                @endforeach
            </tbody>
            
               
            
        </table>
        <div class="text-right">
         {{ $products->links() }}
        </di>
        
        
        <br/>
        <br/>
        <br/>
        <br/>
        
    </div>
</div>

@endsection
