@extends('admin.layouts.master')
@section('content')

<div class="container">
    <div class="row">
        <br/>
        <br/>
        <br/>
        <br>
        <div class="well well-sm">Listagem de avaliações </div>
        <table class="table table-condensed table-hover table-bordered">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>AVALIADO POR</th>
                    <th>DESCRIÇÃO</th>
                    <th>NOTA</th>
                    <th>AÇÃO</th>
                </tr>

            </thead>
            <tbody>
                @foreach($ratings as $rating)
                <tr>
                    <td>{{ $rating->id }}</td>
                    <td>{{ $rating->name }}</td>
                    <td>{{ $rating->description }}</td>
                    <td>{{ $rating->value }}</td>
                    <td>                        
                        <a href="{{ url('admin/ratings-delete', [$rating->id]) }}" class="btn btn-danger">Deletar</a>
                    </td>
                </tr>
                @endforeach
            </tbody>



        </table>
        <div class="text-right">
            {{ $ratings->links() }}
            </di>

            <br/>
            <br/>
            <br/>
            <br/>

        </div>
    </div>

    @endsection
