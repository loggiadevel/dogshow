<div class="navbar navbar-default navbar-fixed-top navbar-inverse">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-ex-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a href="{{ url('/home') }}" class="navbar-brand">DogShow</a>
        </div>
        <div class="collapse navbar-collapse" id="navbar-ex-collapse">
            <ul class="nav navbar-nav navbar-right">
                <li class="active">
                    <a href="#"><i class="fa fa-fw fa-home"></i> Banners</a>
                </li>               
                <li>
                    <a href="#"><i class="fa fa-fw fa-phone"></i> Artigos</a>
                </li>
                <li>
                    <a href="#"><i class="fa fa-fw fa-phone"></i> Produtos</a>
                </li>                   
                <li>
                    <a href="#"><i class="fa fa-fw fa-phone"></i> Avaliações</a>
                </li>
                <li>
                    <a href="#"><i class="fa fa-fw fa-align-justify"></i> Usuários</a>
                </li>

                <!-- Right Side Of Navbar -->
                <ul class="nav navbar-nav navbar-right">
                    <!-- Authentication Links -->
                    @if (Auth::guest())
                    <li><a href="{{ url('/login') }}">Administrar</a></li>
                    <li><a href="{{ url('/register') }}">Registrar</a></li>
                    @else
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                            {{ Auth::user()->name }} <span class="caret"></span>
                        </a>

                        <ul class="dropdown-menu" role="menu">
                            <li><a href="{{ url('/logout') }}"><i class="fa fa-btn fa-sign-out"></i>Logout</a></li>
                        </ul>
                    </li>
                    @endif
                </ul>
            </ul>
        </div>
    </div>
</div>

