@extends('site.layouts.master')
@section('content')

<div id="fullcarousel-example" data-interval="false" class="carousel slide" data-ride="carousel">
    <div class="carousel-inner">

        <?php $i = 0; ?>
        @foreach($banners as $banner)
        <div class="item @if($i==0) active @endif">
            <img src="{{ asset($banner->img) }}">
            <div class="carousel-caption">
                <h2>{{ $banner->title }}</h2>
                <p>{{ $banner->description }}</p>
            </div>
        </div>
        <?php $i++; ?>
        @endforeach


    </div>
    <a class="left carousel-control" href="#fullcarousel-example" data-slide="prev"><i class="icon-prev fa fa-angle-left"></i></a>
    <a class="right carousel-control" href="#fullcarousel-example" data-slide="next"><i class="icon-next fa fa-angle-right"></i></a>
</div>
<div class="section">
    <div class="container">
        <div class="row">

            <h1 class="text-center">Listagem de artigos</h1>

            @foreach($articles as $article)

            <div class="col-md-3">
                <img src="{{ asset($article->img) }}" class="img-responsive">
                <h2>{{ $article->title }}</h2>
                <p>{{ $article->description }}</p>
            </div>

            @endforeach

        </div>
    </div>
</div>
<div class="section">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <img src="{{ asset('site/img/caes-chow-chow.jpg') }}" class="img-responsive">
            </div>
            <div class="col-md-6">
                <h1>CHOW CHOW</h1>
                <h3>A sua característica distintiva</h3>
                <p>CHOW CHOW - Esta peculiar raça de cães tem a sua origem na China e é conhecido desde 2000 anos. A sua utilização principal hoje em dia é de guarda e companhia. Trata-se de um cão de tamanho médio, cerca de 50 cm de altura em média, com aspecto de um pequeno leão. A sua característica distintiva é a de ser uma das poucas raças de cães que tem a língua de cor preta azulada ou violeta, também de ter uma forma de andar única.</p>
                <a class="btn btn-lg btn-primary">Click me</a>
            </div>
        </div>
    </div>
</div>
<div class="section">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1 class="text-center">Galeria de produtos</h1>
            </div>
        </div>
        <div class="row">
            @foreach($products as $product)
            <div class="col-md-4">
                <a><img src="{{ asset($product->img) }}" class="img-responsive"></a>
                <h3>{{ $product->name }}</h3>
                <p>{{ $product->description1 }}</p>
                <p>{{ $product->description2 }}</p>
                <h1 class="text-primary">{{ $product->price }}</h1>
            </div>
            @endforeach
        </div>

    </div>
</div>
<div class="section">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <hr>
            </div>
        </div>
    </div>
</div>
<div class="section">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1 class="text-center">Cadastre-se para receber nossas promoções</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-md-offset-3 col-md-6">
                <form role="form">
                    <div class="form-group">
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Entre com seu e-mail">
                            <span class="input-group-btn">
                                <a class="btn btn-primary" type="submit">Enviar</a>
                            </span>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="section">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1 class="text-center">Comentários</h1>
                <p class="text-center">Seu comentário é muito importante para nós</p>
            </div>
        </div>
        <div class="row">

            @foreach($ratings as $rating)


            <div class="col-md-4">
                <div class="col-md-4">
                    <img src="http://pingendo.github.io/pingendo-bootstrap/assets/user_placeholder.png" class="img-circle img-responsive">
                </div>
                <h3 class="text-left">{{ $rating->name }}</h3>
                <p class="text-left">{{ $rating->description }}</p>
                <p><input type="text" value="{{ $rating->value }}" class="rating-loading nota"></p>
                <script>
                    $(document).on('ready', function(){
                        $('.nota').rating({displayOnly: true, step: 0.5});

                    });
                </script>

            </div> 

            @endforeach


        </div>       
    </div>
</div>
<div class="section">
    <div class="container">
        <div class="row">
            <div class="col-md-12">

                <h2 class="text-center">Seu comentário é muito importante para nós</h2>
                <form role="form" action="{{ url('ratings-store') }}" method="POST">

                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                    <div class="form-group has-warning">
                        <label class="control-label" for="name">Nome</label>
                        <input class="form-control" id="name" name="name" placeholder="Digite seu nome e sobrenome" type="text">
                    </div>
                    <div class="form-group has-warning">
                        <label class="control-label">Comentário</label>
                        <textarea name="description" id="description" class="form-control" placeholder="Digite seu comentário"></textarea>
                    </div>

                    <div class="form-group has-warning">
                        <label for="value" class="control-label">Nota</label>
                        <input type="hidden" id="value" name="value" value="" class="rating-loading">
                        <script>
                            $(document).on('ready', function () {
                                $("#value").rating().on("rating.change", function (event, value, caption) {
                                    $("#value").val(value);
                                    $("#value").rating("refresh", {disabled: false, showClear: false});
                                });
                            });
                        </script>
                    </div>

                    <button type="submit" class="btn btn-lg btn-primary">Enviar</button>
                </form>


                @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif


                <br/>
                <br/>
                <br/>
            </div>
        </div>
    </div>
</div>
@endsection
