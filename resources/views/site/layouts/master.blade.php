<html>
    <head>
        <meta name="keywords" content="">
        <link rel="icon" href="{{ asset('loja/favicon.ico') }}">
        <meta name="description" content="Seu aminal com um tratamento vip">
        <title>DogShow</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link href="http://netdna.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.css" rel="stylesheet">
        <link href="{{ asset('site/rating/star-rating.css') }}" media="all" rel="stylesheet" type="text/css" />
        <link href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link href="{{ asset('site/css/style.css') }}" rel="stylesheet" type="text/css">

        <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
        <script type="text/javascript" src="http://netdna.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>

        <script src="{{ asset('site/rating/star-rating.min.js') }}" type="text/javascript"></script>

    </head>
    <body>
        @include('site.layouts.navigation')  
        @yield('content')
        @include('site.layouts.footer')
    </body>
</html>
